/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import com.alanmrace.jimzmlparser.exceptions.Issue;

/**
 *
 * @author Alan
 */
public class IssueListCell extends ListCell<Issue> {

    private static final Logger LOGGER = Logger.getLogger(IssueListCell.class.getName());

    @FXML protected AnchorPane anchorPane;
    
    @FXML protected VBox issueBox;
    
    @FXML
    protected Label issueTitle;
    @FXML
    protected Label issueDescription;

    private FXMLLoader fxmlLoader;

    @Override
    protected void updateItem(Issue issue, boolean empty) {
        super.updateItem(issue, empty);

        if (fxmlLoader == null) {
            fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/IssueListCell.fxml"));
            fxmlLoader.setController(this);

            try {
                fxmlLoader.load();
            } catch (IOException ex) {
                Logger.getLogger(IssueListCell.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //setText(null);
        issueTitle.getStyleClass().clear();
        
        String style = "";
        
        if (issue == null) { // empty || 
            issueTitle.setText("");
            issueDescription.setText("");
            
            setGraphic(null);
        } else {
            LOGGER.log(Level.INFO, "Updating issue " + issue + "in IssueListCell");

            switch(issue.getIssueLevel()) {
                case ERROR:
                    style = "error-issue";
                    break;
                case WARNING:
                    style = "warning-issue";
                    break;
                case SEVERE:
                default:
                    style = "severe-issue";
                    break;
            }
            
            issueTitle.setText(issue.getIssueTitle());
            issueTitle.prefWidthProperty().bind(issueBox.widthProperty());
            issueDescription.setText(issue.getIssueMessage());
            
            issueTitle.getStyleClass().add(style);
            
            setGraphic(anchorPane);
        }
//        System.out.println(issueTitle.getStyleClass() + ": " + ((issue == null)? "" : issue.getIssueTitle()));
    }
}
