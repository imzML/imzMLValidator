package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlparser.event.CVParamAddRemoveEvent;
import com.alanmrace.jimzmlparser.event.CVParamAddedEvent;
import com.alanmrace.jimzmlparser.event.CVParamChangeEvent;
import com.alanmrace.jimzmlparser.event.CVParamRemovedEvent;
import com.alanmrace.jimzmlparser.event.MzMLContentListener;
import com.alanmrace.jimzmlparser.exceptions.*;
import com.alanmrace.jimzmlvalidator.ImzMLValidator;
import com.alanmrace.jimzmlvalidator.ImzMLValidatorListener;
import com.alanmrace.jimzmlparser.exceptions.Issue.IssueLevel;
import com.alanmrace.jimzmlvalidator.exceptions.UncheckableMappingRuleException;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.alanmrace.jimzmlvalidator.options.ValidatorOptions;
import com.alanmrace.jimzmlvalidator.reporting.ImzMLReport;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import com.alanmrace.jimzmlparser.imzml.ImzML;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.HasChildren;
import com.alanmrace.jimzmlparser.mzml.MzML;
import com.alanmrace.jimzmlparser.mzml.MzMLContent;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.mzml.MzMLTag;
import com.alanmrace.jimzmlparser.mzml.Sample;
import com.alanmrace.jimzmlparser.mzml.SampleList;
import com.alanmrace.jimzmlparser.parser.ImzMLHandler;
import com.alanmrace.jimzmlparser.parser.MzMLHeaderHandler;
import com.alanmrace.jimzmlparser.parser.ParserListener;
import com.alanmrace.jimzmlparser.writer.ImzMLHeaderWriter;
import com.alanmrace.jimzmlparser.writer.ImzMLWriter;
import com.alanmrace.jimzmlparser.writer.MzMLWritable;
import com.alanmrace.jimzmlparser.writer.MzMLWriter;
import com.alanmrace.jimzmlparser.writer.WriterListener;
import com.alanmrace.jimzmlvalidator.CVMapping;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.MzMLValidator;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleWithMzMLContentException;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeItem.TreeModificationEvent;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser.ExtensionFilter;

public class ImzMLValidatorController implements Initializable, ImzMLValidatorListener, ParserListener {

    private static final Logger LOGGER = Logger.getLogger(ImzMLValidatorController.class.getName());

    @FXML AnchorPane mainAnchorPane;

    ValidatorOptions options;

    final FileChooser fileChooser = new FileChooser();

    @FXML
    VBox dropBox;

    @FXML VBox leftSideVBox;

    @FXML
    Button validateButton;

    @FXML
    TextField dropLocation;

    @FXML
    HBox optionsBox;

    @FXML
    TreeView treeView;
    private HashMap<MzMLTag, TreeItem<MzMLTag>> treeItemMap;

    @FXML
    TextArea textArea;

    @FXML
    ImageView imageView;

    @FXML
    TextField searchTreeText;

    @FXML
    ListView treeSearchResultsView;

    @FXML
    ListView resultsListView;
    protected ObservableList<Issue> issueList;
    protected FilteredList<Issue> filteredData;
    // Either FilteredList or SortedList, depending on user options
    protected ObservableList<Issue> currentIssueList;

    @FXML
    TableView<CVMappingFile> mappingListView;
    protected ObservableList<TableColumn<CVMappingFile, ?>> mappingTableColumns;
    protected ObservableList<CVMappingFile> mappingListItems;

    @FXML
    TableView<ConditionalMappingFile> conditionalMappingListView;
    protected ObservableList<TableColumn<ConditionalMappingFile, ?>> conditionalMappingTableColumns;
    protected ObservableList<ConditionalMappingFile> conditionalMappingListItems;

    @FXML
    Label progressLabel;
    @FXML
    ProgressBar progressBar;

    @FXML
    CheckBox hideUncheckableRules;
    @FXML
    CheckBox sortIssueList;

    @FXML
    CheckBox skipHashCheckCheckBox;

    private boolean transitionOccured;
    private File fileToValidate;

    private MzML imzML;

    protected Stage issueEditor;
    protected IssueEditorController issueEditorController;

    protected MzMLValidator validator;

    protected boolean imzMLChanged;

    protected CVMappingFile mzMLMappingFile;
    protected CVMappingFile imzMLMappingFile;
    protected CVMappingFile sourceFileMappingFile;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        issueList = FXCollections.observableArrayList();

        fileChooser.getExtensionFilters().add(new ExtensionFilter("Mass Spectrometry Data (*.imzML, *.mzML)", "*.imzML", "*.mzML"));

        leftSideVBox.widthProperty().addListener((event) -> {
            //    System.out.println("WIDTH UPDATE: " + event);
            updateImageSize();
        });

        leftSideVBox.heightProperty().addListener((event) -> {

            //    System.out.println("HEIGHT UPDATE: " + event);
            updateImageSize();
        });

        mainAnchorPane.widthProperty().addListener((event) -> {
            double fullWidth = mainAnchorPane.getWidth();
            //           System.out.println("MAIN ANCHOR?? " + fullWidth);
            leftSideVBox.setMinWidth((fullWidth - 30) * 0.3);
            leftSideVBox.setMaxWidth((fullWidth - 30) * 0.3);
        });

        //resultsListView.setItems(issueList);
        resultsListView.setCellFactory(issueListView -> new IssueListCell());

        filteredData = new FilteredList<>(issueList, p -> true);

        hideUncheckableRules.selectedProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate((Issue issue) -> {
                // If filter text is empty, display all persons.
                if (newValue == null || !newValue) {
                    return true;
                }

                return !(issue instanceof UncheckableMappingRuleException);
            });
        });

        //resultsListView.setItems(filteredData);
        updateIssueList(filteredData);

        treeView.getSelectionModel().selectedIndexProperty().addListener((listener, oldIndex, newIndex) -> {
            TreeItem<MzMLTag> treeItem = (TreeItem) treeView.getSelectionModel().getSelectedItem();

            if(treeItem != null) {
                MzMLTag mzMLTag = treeItem.getValue();

                showContentEditor();

                if (mzMLTag instanceof HasChildren) {
                    contentEditor.initialise(validator, (MzMLContent) mzMLTag);
                    //        ((CVMappingRuleWithMzMLContentException) issue).getMzMLContent());
                } else {
                    if(mzMLTag != null && mzMLTag.getParent() != null) {
                        contentEditor.initialise(validator, (MzMLContent) mzMLTag.getParent());

                        contentEditor.highlightChild(mzMLTag);
                        issueEditor.requestFocus();
                    }
                }
            }
        });

        treeSearchResultsView.getSelectionModel().selectedIndexProperty().addListener((listener, oldIndex, newIndex) -> {
            MzMLTag mzMLTag = (MzMLTag) treeSearchResultsView.getSelectionModel().getSelectedItem();

            if(mzMLTag != null) {
                showContentEditor();

                if (mzMLTag instanceof HasChildren) {
                    contentEditor.initialise(validator, (MzMLContent) mzMLTag);
                    //        ((CVMappingRuleWithMzMLContentException) issue).getMzMLContent());
                } else {
                    contentEditor.initialise(validator, (MzMLContent) mzMLTag.getParent());

                    contentEditor.highlightChild(mzMLTag);
                    issueEditor.requestFocus();
                }
            }
        });

        treeSearchResultsView.managedProperty().bind(treeSearchResultsView.visibleProperty());
        treeView.managedProperty().bind(treeView.visibleProperty());

        searchTreeText.textProperty().addListener((observable, oldValue, newValue) -> {
//            System.out.println(observable);
//            System.out.println(oldValue);
//            System.out.println(newValue);

            if(newValue.isEmpty()) {
                treeView.setVisible(true);
                treeSearchResultsView.setVisible(false);
            } else {
                treeView.setVisible(false);
                treeSearchResultsView.setVisible(true);

                treeSearchResultsView.getItems().clear();

                // Search the tree
                for(TreeItem<MzMLTag> treeItem : treeItemMap.values()) {
                    if(treeItem.getValue().toString().contains(newValue)) {
                        treeSearchResultsView.getItems().add(treeItem.getValue());
                    }
                }
            }
        });

        // Add default values to Mapping list
        mappingTableColumns = mappingListView.getColumns();
        mappingTableColumns.get(0).setCellValueFactory(new PropertyValueFactory<>("include"));
        mappingTableColumns.get(0).setCellFactory(tc -> new CheckBoxTableCell<>());
        mappingTableColumns.get(1).setCellValueFactory(new PropertyValueFactory<>("filename"));
        mappingTableColumns.get(2).setCellValueFactory(new PropertyValueFactory<>("version"));

        try {

            if((Paths.get("MappingFiles", "ms-mapping.xml").toFile().isFile())) {
                mzMLMappingFile = new CVMappingFile(Paths.get("MappingFiles", "ms-mapping.xml").toString());
            } else {
                mzMLMappingFile = new CVMappingFile(ImzMLValidator.class.getResource("/mapping/ms-mapping.xml"));
            }

            if((Paths.get("MappingFiles", "Ims1.1-mapping.xml").toFile().isFile())) {
                imzMLMappingFile = new CVMappingFile(Paths.get("MappingFiles", "Ims1.1-mapping.xml").toString());
            } else {
                imzMLMappingFile = new CVMappingFile(ImzMLValidator.class.getResource("/mapping/Ims1.1-mapping.xml"));
            }

            if((Paths.get("MappingFiles", "sourceFile-may-mapping.xml").toFile().isFile())) {
                sourceFileMappingFile = new CVMappingFile(Paths.get("MappingFiles", "sourceFile-may-mapping.xml").toString());
            } else {
                sourceFileMappingFile = new CVMappingFile(ImzMLValidator.class.getResource("/mapping/sourceFile-may-mapping.xml"));
            }

            mappingListItems = FXCollections.observableArrayList(mzMLMappingFile, imzMLMappingFile, sourceFileMappingFile);

            mappingListView.setItems(mappingListItems);
            mappingListView.setEditable(true);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Add default values to conditional mapping list
        conditionalMappingTableColumns = conditionalMappingListView.getColumns();
        conditionalMappingTableColumns.get(0).setCellValueFactory(new PropertyValueFactory<>("include"));
        conditionalMappingTableColumns.get(0).setCellFactory(tc -> new CheckBoxTableCell<>());
        conditionalMappingTableColumns.get(1).setCellValueFactory(new PropertyValueFactory<>("filename"));
        conditionalMappingTableColumns.get(2).setCellValueFactory(new PropertyValueFactory<>("version"));

        // Look for any XML files in the current directory and add them to the conditional mapping list
        File dir = new File("ConditionalMappingFiles");
        File [] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".xml");
            }
        });

        try {
            conditionalMappingListItems = FXCollections.observableArrayList(); //new ConditionalMappingFile("MSIMinimumReporting.xml"));

            if(files != null) {
                for (File xmlfile : files) {
                    ConditionalMappingFile mappingFile = new ConditionalMappingFile(xmlfile.getAbsolutePath());

                    if (mappingFile.getConditionalMapping() != null)
                        conditionalMappingListItems.add(mappingFile);
                    else
                        System.out.println("Error adding the conditional mapping file " + xmlfile.getAbsolutePath());
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
            conditionalMappingListItems = FXCollections.observableArrayList();
        }

        conditionalMappingListView.setItems(conditionalMappingListItems);
        conditionalMappingListView.setEditable(true);
    }

    public void stop() {
        if (issueEditor != null) {
            issueEditor.close();
        }
    }

    public void setOptions(ValidatorOptions options) {
        this.options = options;
    }

    @FXML
    void addMappingFileClicked(ActionEvent event) {
        FileChooser mappingFileChooser = new FileChooser();

        try {
            mappingFileChooser.setInitialDirectory(new File(ImzMLValidatorController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
        } catch (URISyntaxException ex) {
            Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
            mappingFileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }

        mappingFileChooser.getExtensionFilters().add(new ExtensionFilter("Mapping file (*.xml)", "*.xml"));
        File file = mappingFileChooser.showOpenDialog(dropLocation.getScene().getWindow());

        if (file != null) {
            try {
                CVMappingFile mappingFile = new CVMappingFile(file.getAbsolutePath());
                mappingListItems.add(mappingFile);
                validator.addCVMapping(mappingFile.getCVMapping());
                mappingFile.includeProperty().addListener((listenerEvent) -> {
                    if (mappingFile.includeProperty().getValue()) {
                        validator.addCVMapping(mappingFile.getCVMapping());
                    } else {
                        validator.removeCVMapping(mappingFile.getCVMapping());
                    }
                });
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);

                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("File not found");
                alert.setHeaderText("Could not find the file " + file.getAbsolutePath());

                alert.showAndWait();
            }
        }
    }

    @FXML
    void addConditionalMappingFileClicked(ActionEvent event) {
        FileChooser mappingFileChooser = new FileChooser();

        try {
            mappingFileChooser.setInitialDirectory(new File(ImzMLValidatorController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
        } catch (URISyntaxException ex) {
            Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
            mappingFileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }

        mappingFileChooser.getExtensionFilters().add(new ExtensionFilter("Conditional mapping file (*.xml)", "*.xml"));
        File file = mappingFileChooser.showOpenDialog(dropLocation.getScene().getWindow());

        if (file != null) {
            try {
                ConditionalMappingFile mappingFile = new ConditionalMappingFile(file.getAbsolutePath());

                conditionalMappingListItems.add(mappingFile);
                validator.addConditionalMapping(mappingFile.getConditionalMapping());
                mappingFile.includeProperty().addListener((listenerEvent) -> {
                    if (mappingFile.includeProperty().getValue()) {
                        validator.addConditionalMapping(mappingFile.getConditionalMapping());
                    } else {
                        validator.removeConditionalMapping(mappingFile.getConditionalMapping());
                    }
                });
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);

                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("File not found");
                alert.setHeaderText("Could not find the file " + file.getAbsolutePath());

                alert.showAndWait();
            }
        }
    }

    @FXML
    protected void fileBoxDragOver(DragEvent event) {
        Dragboard db = event.getDragboard();
        if (db.hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY);
        } else {
            event.consume();
        }
    }

    @FXML
    protected void hideUncheckableRulesAction(ActionEvent event) {

    }

    protected void updateIssueList(ObservableList<Issue> issueList) {
        if (issueEditorController != null) {
            issueEditorController.intitialiseIssueList(issueList);
        }

        currentIssueList = issueList;
        resultsListView.setItems(issueList);
    }

    MzMLContentEditor contentEditor;

    protected void showContentEditor() {
        if (issueEditor == null) {
            contentEditor = new MzMLContentEditor();

            Scene scene = new Scene(contentEditor);

            issueEditor = new Stage();
            //issueEditor.setMinWidth(600);
            //issueEditor.setMinHeight(400);
            issueEditor.setScene(scene);

            resultsListView.getSelectionModel().selectedIndexProperty().addListener((listener, oldIndex, newIndex) -> {
                selectIssue((int) newIndex);
            });

            //selectIssue(resultsListView.getSelectionModel().selectedIndexProperty().get());
            /*try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/IssueEditor.fxml"));
                    //Scene scene = loader.load();
                    Parent root = loader.load(); //getClass().getResource("/fxml/IssueEditor.fxml")

                    Scene scene = new Scene(root);

                    issueEditor = new Stage();
                    issueEditor.setScene(scene);
                    issueEditor.setTitle("Issue Resolver");

                    issueEditorController = loader.<IssueEditorController>getController();

                    resultsListView.getSelectionModel().selectedIndexProperty().addListener((listener, oldIndex, newIndex) -> {
                        selectIssue((int) newIndex);
                    });

                    updateIssueList(resultsListView.getItems());
                } catch (IOException ex) {
                    Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
                }*/
        }

        issueEditor.show();

//            ImzMLIssue issue = issueList.get(resultsListView.getSelectionModel().getSelectedIndex());
//            System.out.println(issue);
    }

    @FXML
    protected void listViewClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            showContentEditor();
            selectIssue(resultsListView.getSelectionModel().getSelectedIndex());
        }
    }

    private void selectIssue(int newIndex) {
        if (newIndex >= 0) {
            //issueEditorController.selectIssue((int) newIndex);
            Issue issue = currentIssueList.get(newIndex);

            LOGGER.log(Level.INFO, "Selected issue {0}", issue);

            // TODO: Think about reorganising this so that all 'exceptions' are just 'Issues'
            if(issue instanceof NonFatalParseIssue) {
                NonFatalParseIssue parseIssue = (NonFatalParseIssue) issue;

                LOGGER.log(Level.INFO, "Issue location {0}", parseIssue.getIssueLocation());

                treeView.getSelectionModel().select(treeItemMap.get(parseIssue.getIssueLocation()));
            } else if(issue instanceof CVMappingRuleWithMzMLContentException) {
                CVMappingRuleWithMzMLContentException cvIssue = (CVMappingRuleWithMzMLContentException) issue;

                LOGGER.log(Level.INFO, "Issue location {0}", cvIssue.getMzMLContent());

                treeView.getSelectionModel().select(treeItemMap.get(cvIssue.getMzMLContent()));
            } else {
                LOGGER.log(Level.INFO, "Not handling issue " + issue);
            }
        }
    }

    @FXML
    protected void sortIssueListAction(ActionEvent event) {
        if (sortIssueList.isSelected()) {
            SortedList<Issue> sortedData = new SortedList<>(filteredData,
                    (Issue issue1, Issue issue2) -> {
                        if (issue1.getIssueLevel().equals(issue2.getIssueLevel())) {
                            return 0;
                        } else if (issue1.getIssueLevel().equals(IssueLevel.SEVERE)) {
                            return -1;
                        } else if (issue2.getIssueLevel().equals(IssueLevel.SEVERE)) {
                            return 1;
                        } else if (issue1.getIssueLevel().equals(IssueLevel.ERROR)) {
                            return -1;
                        } else if (issue2.getIssueLevel().equals(IssueLevel.ERROR)) {
                            return 1;
                        } else if (issue1.getIssueLevel().equals(IssueLevel.WARNING)) {
                            return -1;
                        } else if (issue2.getIssueLevel().equals(IssueLevel.WARNING)) {
                            return 1;
                        }

                        return 1;
                    });

            //resultsListView.setItems(sortedData);
            updateIssueList(sortedData);
        } else {
            //resultsListView.setItems(filteredData);
            updateIssueList(filteredData);
        }

        event.consume();
    }

    private void performTransition() {
        if (transitionOccured) {
            return;
        }

        TranslateTransition moveDropBoxTransition = new TranslateTransition();
        moveDropBoxTransition.setDuration(Duration.millis(500));
        moveDropBoxTransition.setNode(dropBox);
        moveDropBoxTransition.setToY(10 - dropBox.getLayoutY());

//        moveDropBoxTransition.play();
        FadeTransition listViewFade = new FadeTransition(Duration.millis(500));
        listViewFade.setNode(optionsBox);
        listViewFade.setFromValue(0.0);
        listViewFade.setToValue(1.0);
//        listViewFade.playFromStart();

        FadeTransition buttonFade = new FadeTransition(Duration.millis(500));
        buttonFade.setNode(validateButton);
        buttonFade.setFromValue(0.0);
        buttonFade.setToValue(1.0);

        ParallelTransition faders = new ParallelTransition(listViewFade, buttonFade);

        SequentialTransition fullTransition = new SequentialTransition(moveDropBoxTransition, faders);
        fullTransition.play();

        // When the animation has finished set the opacity to be 1 for the remainder
        fullTransition.setOnFinished((ActionEvent event) -> {
            validateButton.setStyle("-fx-opacity: 1.0;");
            optionsBox.setStyle("-fx-opacity: 1.0;");
        });

        transitionOccured = true;
    }

    @FXML
    public void fileBoxClicked(MouseEvent event) {
        File file = fileChooser.showOpenDialog(dropLocation.getScene().getWindow());

        if (file != null) {
            fileToValidate = file;

            fileSelected(fileToValidate);
        }
    }

    Thread.UncaughtExceptionHandler errorHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread th, Throwable exception) {
            try {
                Platform.runLater(() -> {
                    System.out.println("Caught " + exception);

                    System.out.println("Printing stack trace:");
                    StackTraceElement[] elements = exception.getStackTrace();
                    for (int i = 1; i < elements.length; i++) {
                        StackTraceElement s = elements[i];
                        System.out.println("\tat " + s.getClassName() + "." + s.getMethodName()
                                + "(" + s.getFileName() + ":" + s.getLineNumber() + ")");
                    }

                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error occured during validation");
                    alert.setHeaderText("Error occured during validation of " + fileToValidate.getAbsolutePath());
                    alert.setContentText(exception.getLocalizedMessage());

                    // Create expandable Exception.
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    exception.printStackTrace(pw);
                    String exceptionText = sw.toString();

                    Label label = new Label("The exception stacktrace was:");

                    TextArea textArea = new TextArea(exceptionText);
                    textArea.setEditable(false);
                    textArea.setWrapText(true);

                    textArea.setMaxWidth(Double.MAX_VALUE);
                    textArea.setMaxHeight(Double.MAX_VALUE);
                    GridPane.setVgrow(textArea, Priority.ALWAYS);
                    GridPane.setHgrow(textArea, Priority.ALWAYS);

                    GridPane expContent = new GridPane();
                    expContent.setMaxWidth(Double.MAX_VALUE);
                    expContent.add(label, 0, 0);
                    expContent.add(textArea, 0, 1);

                    // Set expandable Exception into the dialog pane.
                    alert.getDialogPane().setExpandableContent(expContent);

                    alert.showAndWait();
                });
            } catch (Throwable t) {
                System.out.println(t);
            }
        }
    };

    @FXML
    public void validateButtonAction(ActionEvent event) {
        // Disable the 'Validate' button so only one validation occurs at once
        validateButton.setDisable(true);
        issueList.clear();

        Runnable task = () -> {
            validator.setFile(fileToValidate.getAbsolutePath(), imzML);

            // Add mapping files
            validator.clearAllMapping();
            for (CVMappingFile cvMappingFile : mappingListItems) {
                if (cvMappingFile.includeProperty().get()) {
                    validator.addCVMapping(cvMappingFile.getCVMapping());
                }
            }

            for (ConditionalMappingFile conditionalMappingFile : conditionalMappingListItems) {
                if (conditionalMappingFile.includeProperty().get()) {
                    validator.addConditionalMapping(conditionalMappingFile.getConditionalMapping());
                }
            }

            if (validator instanceof ImzMLValidator) {
                ((ImzMLValidator) validator).setPerformHashCheck(!skipHashCheckCheckBox.isSelected());
            }

            System.out.println("Validating using: " + validator);
            validator.validate();

//            Collection<ImzMLIssue> issues = validator.getIssues();
//            IssueReport report = new IssueReport(issues);
//            TextReport textReport = new TextReport(report);
            Platform.runLater(() -> {

//                for(ImzMLIssue issue : report) {
//                    issueList.add(issue);
//                }
                progressBar.setProgress(1.0);
                validateButton.setDisable(false);
            });
        };

        Thread thread = new Thread(task);
        thread.setUncaughtExceptionHandler(errorHandler);
        thread.start();
    }

    @FXML
    public void fileBoxDragDropped(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;

        if (db.hasFiles()) {
            File newFileToValidate = null;
            String fullPath = null;

            for (File file : db.getFiles()) {
                fullPath = file.getAbsolutePath();
                int extensionIndex = fullPath.lastIndexOf('.');

                if (extensionIndex > 0) {
                    String extension = fullPath.substring(extensionIndex + 1);

                    if (extension.equalsIgnoreCase("imzML") || extension.equalsIgnoreCase("mzML")) {
                        newFileToValidate = file;
                        success = true;

//                        System.out.println("ImzMLValidatorController#fileBoxDragDropped(): " + fullPath);

                        break;
                    }
                }
            }

            if(newFileToValidate != null) {
                fileToValidate = newFileToValidate;
                System.out.println("Validating: " + fileToValidate);
                fileSelected(fileToValidate);
            } else {
                // Display error message
                new Alert(Alert.AlertType.ERROR, "File not supported " + fullPath).showAndWait();
            }
        }

        event.setDropCompleted(success);
        event.consume();
    }

    @FXML
    public void saveButtonClicked(ActionEvent event) {
        dropLocation.setDisable(true);
        optionsBox.setDisable(true);
        progressLabel.setText("Preparing to write imzML...");

        // TODO: Move this to the content editor?
        if(this.contentEditor.mzMLContent != null)
            this.contentEditor.mzMLContent.removeAllListeners();
        this.issueEditor.close();
        issueEditor = null;

        Runnable task = () -> {
            List<MzMLContentListener> listeners = imzML.getListeners();
            imzML.removeAllListeners();

            MzMLWriter imzMLWriter = null;

            if(imzML instanceof ImzML) {
                imzMLWriter = new ImzMLHeaderWriter();
                imzMLWriter.shouldOutputIndex(false);
            } else if(imzML instanceof MzML) {
                // TODO: MzML saving
                imzMLWriter = new MzMLWriter();
            }

            if(imzMLWriter != null) {
                addWriterListener(imzML, imzMLWriter);

                try {
                    imzMLWriter.write(imzML, fileToValidate.getAbsolutePath());
                } catch (IOException ex) {
                    Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            for (MzMLContentListener listener : listeners)
                imzML.addListener(listener);

            // Reload the file to ensure any errors are updated
            Platform.runLater(() -> {
                fileSelected(new File(fileToValidate.getAbsolutePath()));
            });
        };

        Thread thread = new Thread(task);
        thread.setUncaughtExceptionHandler(errorHandler);
        thread.start();
    }

    private void addWriterListener(MzML imzML, MzMLWritable imzMLWriter) {
        imzMLWriter.addListener(new WriterListener() {
            @Override
            public void start() {
                Platform.runLater(() -> {
                    dropLocation.setDisable(true);
                    optionsBox.setDisable(true);
                });
            }

            @Override
            public void progress(long current) {
                Platform.runLater(() -> {
                    progressLabel.setText("Saving spectrum " + current);
                    progressBar.setProgress((current * 1.0) / imzML.getRun().getSpectrumList().size());
                });
            }

            @Override
            public void end() {
                Platform.runLater(() -> {
                    dropLocation.setDisable(false);
                    optionsBox.setDisable(false);
                    validateButton.setDisable(false);
                });
            }

        });
    }

    @FXML
    public void saveAsButtonClicked(ActionEvent event) {
        String filename = fileToValidate.getAbsolutePath();
        File previousFile = new File(filename);

        fileChooser.setInitialDirectory(previousFile.getParentFile());
        fileChooser.setInitialFileName(filename);
        File file = fileChooser.showSaveDialog(dropLocation.getScene().getWindow());

        if (file != null) {
            dropLocation.setDisable(true);
            optionsBox.setDisable(true);
            progressLabel.setText("Preparing to write imzML...");

            // TODO: Move this to the content editor?
            if(this.contentEditor != null && this.contentEditor.mzMLContent != null)
                this.contentEditor.mzMLContent.removeAllListeners();

            if(this.issueEditor != null) {
                this.issueEditor.close();
                issueEditor = null;
            }

            Runnable task = () -> {
                // Remove listeners as writing to imzML can make changes to the parameters
                List<MzMLContentListener> listeners = imzML.getListeners();
                imzML.removeAllListeners();

                MzMLWritable imzMLWriter;

                if(imzML instanceof ImzML) {
                    if(previousFile.getAbsolutePath().equals(file.getAbsolutePath()))
                        imzMLWriter = new ImzMLHeaderWriter();
                    else
                        imzMLWriter = new ImzMLWriter();

                    imzMLWriter.shouldOutputIndex(false);
                } else {
                    imzMLWriter = new MzMLWriter();
                }

                addWriterListener(imzML, imzMLWriter);

                try {
                    imzMLWriter.write(imzML, file.getAbsolutePath());

                    fileToValidate = new File(file.getAbsolutePath());

                    Platform.runLater(() -> {
                        fileSelected(fileToValidate);
                    });
                } catch (IOException ex) {
                    Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
                }

                // Re-add the listeners back to imzML
                for(MzMLContentListener listener : listeners)
                    imzML.addListener(listener);
            };

            Thread thread = new Thread(task);
            thread.setUncaughtExceptionHandler(errorHandler);
            thread.start();
        }
    }

    protected void fileSelected(File fileToValidate) {
        dropLocation.setText("Loading imzML...");
        dropLocation.setAlignment(Pos.CENTER_LEFT);
        dropLocation.setDisable(true);
        issueList.clear();

        //fileListView.setItems(fileList);
        if (!transitionOccured) {
            performTransition();
        }

        optionsBox.setDisable(true);

        Runnable task = () -> {
            try {
                String fullPath = fileToValidate.getAbsolutePath();
                int extensionIndex = fullPath.lastIndexOf('.');

                if (extensionIndex > 0) {
                    String extension = fullPath.substring(extensionIndex + 1);

                    // Reset any ignored rules
                    for(CVMappingFile mappingFile :  this.mappingListItems) {
                        for(CVMappingRule rule : mappingFile.getCVMapping().getCVMappingRuleList()) {
                            rule.clearIgnore();
                        }
                    }

                    for(ConditionalMappingFile conditionalMappingFile :  this.conditionalMappingListItems) {
                        for(CVMapping mapping : conditionalMappingFile.getConditionalMapping().getFullMappingList()) {
                            for(CVMappingRule rule : mapping.getCVMappingRuleList())
                                rule.clearIgnore();
                        }
                    }

                    if(extension.equalsIgnoreCase("imzML")) {
                        imzML = ImzMLHandler.parseimzML(fullPath, true, this);

                        if(!mappingListItems.contains(this.imzMLMappingFile))
                            mappingListItems.add(this.imzMLMappingFile);

                        validator = new ImzMLValidator();
                        validator.registerImzMLValidatorListener(this);

                        // Add in a sample if it doesn't exist, so that minimum reporting validation can take place
                        if(imzML != null) {
                            if(imzML.getSampleList() == null)
                                imzML.setSampleList(new SampleList(1));

                            if(imzML.getSampleList().size() < 1) {
                                Sample sample = new Sample();

                                imzML.getSampleList().add(sample);
                            }
                        }
                    } else if(extension.equalsIgnoreCase("mzML")) {
                        imzML = MzMLHeaderHandler.parsemzMLHeader(fullPath, true, this);

                        mappingListItems.remove(this.imzMLMappingFile);

                        validator = new MzMLValidator();
                        validator.registerImzMLValidatorListener(this);
                    }
                }
            } catch (MzMLParseException ex) {
                Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);

                throw(new FatalRuntimeParseException(ex.getIssue(), ex));
            }
//            } catch (Exception ex) {
//                imzML = null;
//                Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
//
//                throw(ex);
//            }

            Platform.runLater(() -> {
                dropLocation.setText(fileToValidate.getName());
                dropLocation.setAlignment(Pos.CENTER_LEFT);
                dropLocation.setDisable(false);

                if (imzML == null) {
                    this.validateButtonAction(null);
                } else {
                    imzMLChanged = false;

                    // Generation of TIC must come first or the tree may not be complete

                    // TODO: only try and generate the TIC image if the parsing of
                    // the imzML has been successful.
                    // Display the TIC
                    if(imzML instanceof ImzML) {
                        try {
                            updateImageView(((ImzML) imzML).generateTICImage());
                        } catch (ArrayIndexOutOfBoundsException ex) {
                            // Failed to generate the TIC image
                            InvalidCVParamValue issue = new InvalidCVParamValue("Invalid coordinates specified in IMS:1000042 (max count of pixels x) or IMS:1000043 (max count of pixels y)"); // " + ex.getLocalizedMessage());
                            issue.setIssueLocation(imzML.getScanSettingsList().getScanSettings(0));

                            //issue.fixAttempted("Save as new imzML file to resolve the issue.");

                            this.issueFound(issue);
                        }
                    } else
                        updateImageView(null);

                    TreeItem<MzMLTag> root = new TreeItem<>(imzML);
                    root.setExpanded(true);

                    treeItemMap = createTreeItemMap(imzML, root);
                    treeView.setRoot(root);

                    //ImzMLReport report = ImzMLReportHandler.parseImzMLReport(ImzMLValidatorController.class.getResourceAsStream("/imzMLReport.xml"));

                    // TODO: Write a mzML version of the report?
                    if(imzML instanceof ImzML && options != null) {
                        ImzMLReport report = options.getReport();
                        textArea.setText(report.processReport(imzML));
                    } else {
                        textArea.setText("");
                    }

                    imzML.addListener((event) -> {
                        imzMLChanged = true;

                        // TODO: Add in the update to the tree here
                        TreeItem<MzMLTag> selectedItem = (TreeItem<MzMLTag>) treeView.getSelectionModel().getSelectedItem();
                        System.out.println("Event: " + event);
                        System.out.println("Event source: " + event.getSource() + " (" + System.identityHashCode(event.getSource()) + ")");
                        System.out.println("Selected item: " + selectedItem);

//                        TreeItem<MzMLTag> treeRoot = new TreeItem<>(imzML);
//                        treeRoot.setExpanded(true);

//                            treeItemMap = createTreeItemMap(imzML, treeRoot);
//                            treeView.setRoot(treeRoot);

                        MzMLTag source = event.getSource();

                        // Get the previously selected item in the newly created map
                        selectedItem = treeItemMap.get(source);
                        //treeView.getSelectionModel().select(selectedItem);

                        // Update the part of the tree that has changed
                        if(event instanceof CVParamChangeEvent && selectedItem != null) {
                            TreeModificationEvent<MzMLTag> ev = new TreeModificationEvent<>(TreeItem.valueChangedEvent(), selectedItem, true);
                            Event.fireEvent(selectedItem, ev);
                        }

                        if(event instanceof CVParamAddedEvent) {
                            CVParam cvParam = ((CVParamAddedEvent) event).getCVParam();

                            TreeItem<MzMLTag> childNode = new TreeItem<>(cvParam);
                            treeItemMap.put(cvParam, childNode);

                            treeItemMap.get(cvParam.getParent()).getChildren().add(childNode);
                        }

                        if(event instanceof CVParamRemovedEvent) {
                            CVParam cvParam = ((CVParamRemovedEvent) event).getCVParam();

                            TreeItem<MzMLTag> removedItem = treeItemMap.remove(cvParam);
                            treeItemMap.get(cvParam.getParent()).getChildren().remove(removedItem);
                        }

                        if(selectedItem != null) {
                            // If the selected value is a CVParam then scroll only to the parent
                            // for usability
                            if (selectedItem.getValue() instanceof CVParam)
                                selectedItem = selectedItem.getParent();

                            expandTreeTo(selectedItem);
                            treeView.scrollTo(treeView.getRow(selectedItem));
                        }

                        if (event instanceof CVParamAddRemoveEvent) {
                            for (Iterator<Issue> iterator = issueList.iterator(); iterator.hasNext(); ) {
                                Issue issue = iterator.next();

                                if (issue instanceof CVMappingRuleWithMzMLContentException) {
                                    MzMLContentWithParams mzMLContent = ((CVMappingRuleWithMzMLContentException) issue).getMzMLContent();
                                    CVMappingRule mappingRule = ((CVMappingRuleWithMzMLContentException) issue).getCVMappingRule();

                                    if (mzMLContent != null) {
                                        if (mzMLContent.equals(((CVParamAddRemoveEvent) event).getSource())) {
                                            try {
                                                if (mappingRule.check(mzMLContent)) {
                                                    // TODO: Keep updated by checking the rules again - this way doesn't re-add bad rules
                                                    iterator.remove();
                                                    //                                                    issueEditor.hide();
                                                }
                                            } catch (CVMappingRuleException ex) {
                                                // Don't need to do anything with this error.
                                                //Logger.getLogger(ImzMLValidatorController.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });
                }

                optionsBox.setDisable(false);
            });
        };

        Thread thread = new Thread(task);
        thread.setUncaughtExceptionHandler(errorHandler);
        thread.start();
    }

    private void expandTreeTo(TreeItem<MzMLTag> tag) {
        if(tag.getParent() != null)
            expandTreeTo(tag.getParent());

        tag.setExpanded(true);
    }

    public static HashMap<MzMLTag, TreeItem<MzMLTag>> createTreeItemMap(MzML imzML, TreeItem<MzMLTag> root) {
        HashMap<MzMLTag, TreeItem<MzMLTag>> treeItemMap = new HashMap<>();

        treeItemMap.put(imzML, root);

        addChild(treeItemMap, root, imzML);

        return treeItemMap;
    }

    protected static void addChild(HashMap<MzMLTag, TreeItem<MzMLTag>> treeItemMap, TreeItem<MzMLTag> root, MzMLTag parent) {
        List<MzMLTag> children = new LinkedList<>();

        if (parent instanceof HasChildren) {
            ((HasChildren) parent).addChildrenToCollection(children);
        }

        // Sort the children such that the CVParams are after all other child tags
        // (opposite order to how it is in the XML, but easier for navagating around
        children.sort((x, y) -> {
            if (x.getClass().equals(y.getClass()) && !(x instanceof CVParam)) {
                return 0;
            }

            if (x instanceof CVParam && !(y instanceof CVParam)) {
                return 1;
            } else if (y instanceof CVParam && !(x instanceof CVParam)) {
                return -1;
            }

            return (children.indexOf(x) > children.indexOf(y)) ? 1 : -1;
        });

        for (MzMLTag child : children) {
            if (child == null) {
                continue;
            }

            TreeItem<MzMLTag> childNode = new TreeItem<>(child);
            treeItemMap.put(child, childNode);

            root.getChildren().add(childNode);

            addChild(treeItemMap, childNode, child);
        }
    }

    private void updateImageView(double[][] imageData) {
        if(imageData == null) {
            imageView.setImage(null);
            return;
        }

        double maxValue = 0;

        for (int y = 0; y < imageData.length; y++) {
            for (int x = 0; x < imageData[0].length; x++) {
                if (imageData[y][x] > maxValue) {
                    maxValue = imageData[y][x];
                }
            }
        }

        int dim1Length = imageData.length;
        int dim2Length;

        if (imageData.length == 0) {
            dim2Length = 0;
        } else {
            dim2Length = imageData[0].length;
        }

        if (dim1Length > 0 && dim2Length > 0) {
            int[] ticImage = new int[dim1Length * dim2Length];

            for (int y = 0; y < dim1Length; y++) {
                for (int x = 0; x < dim2Length; x++) {
                    int index = y * dim2Length + x;
                    int value = (int) Math.round(imageData[y][x] / maxValue * 255);

                    int alpha = 255;
                    int r, g, b;
                    r = value;
                    g = value;
                    b = value;

                    ticImage[index] = 0;

                    ticImage[index] |= (alpha << 24);
                    ticImage[index] |= (r << 16);
                    ticImage[index] |= (g << 8);
                    ticImage[index] |= (b);

                }
            }

            WritableImage image = new WritableImage(dim2Length, dim1Length);

            PixelWriter pw = image.getPixelWriter();
            pw.setPixels(0, 0, dim2Length, dim1Length, PixelFormat.getIntArgbInstance(), ticImage, 0, imageData[0].length);

            imageView.setImage(image);
        } else {
            imageView.setImage(null);
        }

        updateImageSize();
    }

    private void updateImageSize() {
        if (imageView.getImage() != null) {
            double imageWidth = imageView.getImage().getWidth();
            double imageHeight = imageView.getImage().getHeight();
            double ratio = imageHeight / imageWidth;

            double newHeight = leftSideVBox.getWidth() * ratio;

//            System.out.println("Updating image size");

            // At most use 25% of the VBox to display the TIC image
            if (newHeight > leftSideVBox.getHeight() * 0.25) {
                double newImageHeight = leftSideVBox.getHeight() * 0.25;

//                System.out.println("Height: " + newImageHeight);

                if(imageHeight < newImageHeight)
                    imageView.setImage(resample(imageView.getImage(), (int)Math.ceil(newImageHeight / imageHeight)));

//                imageView.setFitHeight(newImageHeight);
            } else {
                // TODO: Store the underlying data to avoid recreating the image?

                if(imageWidth < leftSideVBox.getWidth())
                    imageView.setImage(resample(imageView.getImage(), (int)Math.ceil(leftSideVBox.getWidth() / imageWidth)));

//                System.out.println("Setting width to " + leftSideVBox.getWidth());
            }

            imageView.setFitWidth(leftSideVBox.getWidth());
        }
    }

    // Taken from: https://gist.github.com/jewelsea/5415891
    private Image resample(Image input, int scaleFactor) {
        final int W = (int) input.getWidth();
        final int H = (int) input.getHeight();
        final int S = scaleFactor;

        WritableImage output = new WritableImage(
                W * S,
                H * S
        );

        PixelReader reader = input.getPixelReader();
        PixelWriter writer = output.getPixelWriter();

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                final int argb = reader.getArgb(x, y);
                for (int dy = 0; dy < S; dy++) {
                    for (int dx = 0; dx < S; dx++) {
                        writer.setArgb(x * S + dx, y * S + dy, argb);
                    }
                }
            }
        }

        return output;
    }

    @Override
    public void startingStep(ImzMLValidator.ValidatorStep step) {
        Platform.runLater(() -> {
            String message = "";
            double progress = 0;

            switch (step) {
                case VALIDATE_XML:
                    message = "Validating XML";
                    progress = 0;
                    break;
                case VALIDATE_IMZML:
                    message = "Validating ImzML against mzML XSD";
                    progress = 0.20;
                    break;
                case VALIDATE_MAPPING:
                    message = "Validating ImzML against mapping file(s)";
                    progress = 0.40;
                    break;
//                case ValidateImzMLMapping:
//                    message = "Validating ImzML against imzML mapping file";
//                    progress = 0.60;
//                    break;
                case CHECK_UUID:
                    message = "Checking UUID matches.";
                    progress = 0.80;
                    break;
                case CHECK_HASH:
                    message = "Checking integrity of data (hash check).";
                    progress = 0.90;
                    break;
                case COMPLETE:
                    message = "";
                    progress = 1;
                    break;
            }

            progressLabel.setText(message);
            progressBar.setProgress(progress);
        });
    }

    @Override
    public void finishingStep(ImzMLValidator.ValidatorStep step) {
        Platform.runLater(() -> {
            if(step.equals(ImzMLValidator.ValidatorStep.COMPLETE))
                progressLabel.setText("[Done]");
            else
                progressLabel.setText("[Done] " + progressLabel.getText());
        });

    }

    private long lastScrollToOccured;

    @Override
    public void issueFound(Issue issue) {
        LOGGER.log(Level.INFO, String.format("Issue encountered %s", issue.getClass()));

        if(issue != null) {
            Platform.runLater(() -> {
                issueList.add(issue);

                long currentTime = System.nanoTime();

                if ((currentTime - lastScrollToOccured) > 1e8) {
                    resultsListView.scrollTo(issueList.size() - 1);
                    lastScrollToOccured = currentTime;
                }
            });
        }
    }
}
