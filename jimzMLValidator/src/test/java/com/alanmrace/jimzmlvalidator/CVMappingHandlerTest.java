/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alan
 */
public class CVMappingHandlerTest {
    
    private static final Logger logger = Logger.getLogger(ImzMLValidatorTest.class.getName());
    
    public final static String CVMAPPING_RESOURCE = "/mapping/ms-mapping.xml";
    
    public CVMappingHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCVMapping method, of class CVMappingHandler.
     */
    @Test
    public void testGetCVMapping() {
        CVMapping cvMapping = CVMappingHandler.parseCVMapping(CVMappingHandlerTest.class.getResourceAsStream(CVMAPPING_RESOURCE));
        
        System.out.println(cvMapping);
        assertNotNull("CV Mapping File", cvMapping);
    }
    
}
