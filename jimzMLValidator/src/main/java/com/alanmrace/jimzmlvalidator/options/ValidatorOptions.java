package com.alanmrace.jimzmlvalidator.options;

import com.alanmrace.jimzmlvalidator.reporting.ImzMLReport;
import com.alanmrace.jimzmlvalidator.reporting.ImzMLReport.ReportEntry;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Alan Race
 */
public class ValidatorOptions {

    private static final Logger LOGGER = Logger.getLogger(ValidatorOptions.class.getName());

    private String mzMLSchemaURI;
    private String mzMLIDXSchemaURI;
    private String imzMLSchemaURI;
    private String imzMLAllSchemaURI;

    private boolean downloadLatestSchemasOnStart;

    private List<String> mappingURIs;
    private List<String> conditionalMappingURIs;

    private boolean downloadLatestMappingFilesOnStart;

    private String imsOBOURI;
    private String msOBOURI;

    private boolean downloadLatestOBOsOnStart;

    private boolean useInternalMappingFiles;

    private List<String> mappingFiles;
    private List<String> conditionalMappingFiles;

    private boolean skipDataIntegrityCheck;
    private List<ReportEntry> reportEntries;

    public static ValidatorOptions loadFromFile(String filename) throws FileNotFoundException {
        ValidatorOptions options;

        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(filename));
        options = gson.fromJson(reader, ValidatorOptions.class);

        return options;
    }

    public void isFirstRun(boolean isFirstRun) {
        if (isFirstRun) {
            downloadLatestSchemasOnStart = true;
            downloadLatestMappingFilesOnStart = true;
            downloadLatestOBOsOnStart = true;
        }
    }

    public ImzMLReport getReport() {
        ImzMLReport report = new ImzMLReport();

        report.addEntries(reportEntries);

        return report;
    }


    public String getMSOBOURI() {
        return msOBOURI;
    }

    public String getIMSOBOURI() {
        return imsOBOURI;
    }


    public boolean downloadLatestSchemasOnStart() {
        return downloadLatestSchemasOnStart;
    }

    public boolean downloadLatestMappingFilesOnStart() {
        return downloadLatestMappingFilesOnStart;
    }

    public boolean downloadLatestOBOsOnStart() {
        return downloadLatestOBOsOnStart;
    }

    public String getMzMLSchemaURI() {
        return mzMLSchemaURI;
    }

    public String getMzMLIDXSchemaURI() {
        return mzMLIDXSchemaURI;
    }

    public String getImzMLSchemaURI() {
        return imzMLSchemaURI;
    }

    public String getImzMLAllSchemaURI() {
        return imzMLAllSchemaURI;
    }

    public List<String> getConditionalMappingFiles() {
        return conditionalMappingFiles;
    }

    public List<String> getMappingFiles() {
        return mappingFiles;
    }

    public List<String> getMappingURIs() {
        return mappingURIs;
    }

    public List<String> getConditionalMappingURIs() {
        return conditionalMappingURIs;
    }
}
