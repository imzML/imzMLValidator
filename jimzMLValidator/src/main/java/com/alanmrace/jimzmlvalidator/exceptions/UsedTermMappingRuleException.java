/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;

/**
 *
 * @author Alan
 */
public class UsedTermMappingRuleException extends CVTermException {

    public UsedTermMappingRuleException(CVMappingRule mappingRule, MzMLContentWithParams content, CVTerm cvTerm) {
        super(mappingRule, content, cvTerm);
    }

    @Override
    public String getIssueTitle() {
        return "" + cvTerm.getTermAccession() + " used in rule " + mappingRule.getName();
    }
    
    @Override
    public String getIssueMessage() {
        String message = "It is not permitted to use the term " + cvTerm.getTermAccession();
        
        if(cvTerm.getOBOTerm() != null)
            message += " (" + cvTerm.getOBOTerm().getName() + ")";
        
        message += " please select a child term instead.";
        
        return message;
    }
}
