package com.alanmrace.jimzmlvalidator.exceptions;

/**
 *
 * @author Alan Race
 */
public class InvalidHashException extends InvalidXMLIssue {

    /**
     *
     */
    private static final long serialVersionUID = 4797514992158926994L;

    public enum Reason {
        NO_HASH_CV_PARAM,
        UNKNOWN_HASH_ALGORITHM,
        HASH_MISMATCH,
        UNKNOWN
    }

    private final Reason reason;

    public InvalidHashException(Reason reason, String message) {
        super(message, null);
        
        this.reason = reason;
    }
    
    public InvalidHashException(String message, Exception ex) {
    	super(message, ex);
    	
    	this.reason = Reason.UNKNOWN;
    }
    
    @Override
    public String getIssueMessage() {
        if(reason == null)
            return super.getIssueMessage();
        
        return getLocalizedMessage();
    }
    
    @Override
    public String getIssueTitle() {
        if(reason == null)
            return super.getIssueTitle();
        
        return "Invalid ibd Checksum";
    }
}
