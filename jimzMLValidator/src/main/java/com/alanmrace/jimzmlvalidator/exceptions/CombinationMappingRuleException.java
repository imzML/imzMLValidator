/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;

/**
 *
 * @author Alan
 */
public class CombinationMappingRuleException extends CVMappingRuleWithMzMLContentException {

    public CombinationMappingRuleException(CVMappingRule mappingRule, MzMLContentWithParams content) {
        super(mappingRule, content);
    }
    
    

    @Override
    public String getIssueMessage() {
        String message = "";
        
        message += mappingRule.getDescription();
        
        return message;
    }

    @Override
    public String getIssueTitle() {
        return "Mapping file error at rule " + mappingRule.getName();
    }

    
}
