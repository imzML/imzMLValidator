package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.exceptions.UnfollowableXPathException;
import com.alanmrace.jimzmlvalidator.CVMappingRule;

/**
 *
 * @author Alan Race
 */
public class UncheckableMappingRuleException extends CVMappingRuleException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -382794174720161986L;
	
	protected final Exception exception;
    
    public UncheckableMappingRuleException(CVMappingRule mappingRule, Exception exception) {
        super(mappingRule);
        
        this.exception = exception;
    }    

    @Override
    public String getIssueMessage() {
        StringBuilder message = new StringBuilder("Cannot follow the XPath ");
        message.append(mappingRule.getCVElementPath());
        message.append("\n");
        
        String missingTag = "";
        
        if(exception instanceof UnfollowableXPathException) {
            UnfollowableXPathException uxpException = (UnfollowableXPathException) exception;
            
            missingTag = uxpException.getSubXPath().substring(1);
            
            int nextTagLoc = missingTag.indexOf('/');
            
            if(nextTagLoc != -1)
                missingTag = missingTag.substring(0, nextTagLoc);
        } else {   
            missingTag = mappingRule.getScopePath();
            
            missingTag = missingTag.substring(missingTag.lastIndexOf('/') + 1);
        }
                
        message.append("This is likely because a ");
        message.append(missingTag);
        message.append(" tag does not exist.");
        
        return message.toString();
    }

    @Override
    public String getIssueTitle() {
        return "Uncheckable mapping rule " + mappingRule.getName() + " likely due to unfollowable XPath";
    }
    
}
