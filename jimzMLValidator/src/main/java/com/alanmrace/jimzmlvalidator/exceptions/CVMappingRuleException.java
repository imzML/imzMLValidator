package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.exceptions.Issue;
import com.alanmrace.jimzmlvalidator.CVMappingRule;

/**
 *
 * @author Alan Race
 */
public abstract class CVMappingRuleException extends Exception implements Issue {

    /**
	 *
	 */
	private static final long serialVersionUID = -7395290469807824899L;

	protected final CVMappingRule mappingRule;
    
    public CVMappingRuleException(CVMappingRule mappingRule) {
        if(mappingRule == null) {
            throw(new IllegalArgumentException("mappingRule cannot be null"));
        }

        this.mappingRule = mappingRule;
    }
    
    public CVMappingRule getCVMappingRule() {
        return mappingRule;
    }
    
    @Override
    public IssueLevel getIssueLevel() {
        switch(mappingRule.getRequirementLevel()) {
            case MAY:
                return IssueLevel.WARNING;
            case SHOULD:
                return IssueLevel.ERROR;
            case MUST:
            default:
                return IssueLevel.SEVERE;
        }
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + ": (" + this.getIssueTitle() + ") " + this.getIssueMessage();
    }
}
