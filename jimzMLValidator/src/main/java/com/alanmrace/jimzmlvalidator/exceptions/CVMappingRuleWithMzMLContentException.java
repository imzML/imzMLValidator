package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;

/**
 *
 * @author Alan Race
 */
public abstract class CVMappingRuleWithMzMLContentException extends CVMappingRuleException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 3941616948256574930L;
	
	protected final MzMLContentWithParams content;
    
    public CVMappingRuleWithMzMLContentException(CVMappingRule mappingRule, MzMLContentWithParams content) {
        super(mappingRule);

        if(content == null) {
            throw(new IllegalArgumentException("content cannot be null"));
        }
        
        this.content = content;
    }
    
    public MzMLContentWithParams getMzMLContent() {
        return content;
    }
}
