package com.alanmrace.jimzmlvalidator.reporting;

import java.util.Iterator;
import com.alanmrace.jimzmlparser.exceptions.Issue;

/**
 *
 * @author Alan Race
 */
public class TextReport implements Iterable<String> {

    IssueReport report;
    
    public TextReport(IssueReport report) {
        this.report = report;
    } 
    
    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private Iterator<Issue> iterator = report.iterator();

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public String next() {
                Issue currentIssue = iterator.next();
                
                return currentIssue.getIssueMessage();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    
    
}
