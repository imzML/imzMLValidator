package com.alanmrace.jimzmlvalidator.reporting;

import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVMappingRule.RequirementLevel;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.ChildUsedMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.CombinationMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidXMLIssue;
import com.alanmrace.jimzmlvalidator.exceptions.RepeatedMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.UncheckableMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.UsedTermMappingRuleException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import com.alanmrace.jimzmlparser.exceptions.Issue;

/**
 *
 * @author Alan Race
 */
public class IssueReport implements Iterable<Issue> {

    public enum ReportingLevel {
        FULL, // Same as all issues turned on
        ALMOSTFULL, // Same as Everything except UncheckableMapping
        MUST, // Same as InvalidXML, InvalidImzML, and CVTermExceptions (only MUST)
        SHOULD, // Same as InvalidXML, InvalidImzML, and CVTermExceptions (only SHOULD)
        MAY         // Same as InvalidXML, InvalidImzML, and CVTermExceptions (only MAY)
    }

    public enum IssueType {
        INVALID_XML,
        INVALID_IMZML,
        // Rules that cannot be checked due to unfollowable XPath
        UNCHECKABLE_MAPPING,
        // Below are CVTermExceptions
        COMBINATION_MAPPING,
        CHILD_USED_MAPPING,
        USED_TERM_MAPPING,
        REPEATED_MAPPING
    }

    protected Collection<Issue> issues;
    protected Map<IssueType, IssueTriple> issuesToReport;

    public IssueReport(Collection<Issue> issues) {
        this.issues = issues;

        setReportingLevel(ReportingLevel.FULL);
    }

    protected class IssueTriple {

        IssueType type;
        RequirementLevel level;
        boolean report;

        public IssueTriple(IssueType type, boolean report) {
            this(type, null, report);
        }

        public IssueTriple(IssueType type, RequirementLevel level, boolean report) {
            this.type = type;
            this.level = level;
            this.report = report;
        }
    }

    @Override
    public Iterator<Issue> iterator() {
        ArrayList<Issue> issuesReduced = new ArrayList<>();

        for (Issue issue : issues) {
            IssueTriple triple = null;

            if (issue instanceof CVMappingRuleException
                    && !(issue instanceof UncheckableMappingRuleException)) {
                CVMappingRule rule = ((CVMappingRuleException) issue).getCVMappingRule();

                if (issue instanceof CombinationMappingRuleException) {
                    triple = issuesToReport.get(IssueType.COMBINATION_MAPPING);
                } else if (issue instanceof RepeatedMappingRuleException) {
                    triple = issuesToReport.get(IssueType.REPEATED_MAPPING);
                } else if (issue instanceof UsedTermMappingRuleException) {
                    triple = issuesToReport.get(IssueType.USED_TERM_MAPPING);
                } else if (issue instanceof ChildUsedMappingRuleException) {
                    triple = issuesToReport.get(IssueType.CHILD_USED_MAPPING);
                }

                if (triple != null && triple.report) {
                    boolean add = false;

                    switch (triple.level) {
                        case MAY:
                            add = true;
                            break;
                        case SHOULD:
                            if (rule.getRequirementLevel().equals(RequirementLevel.MUST)
                                    || rule.getRequirementLevel().equals(RequirementLevel.SHOULD)) {
                                add = true;
                            }
                            break;
                        case MUST:
                            if (rule.getRequirementLevel().equals(RequirementLevel.MUST)) {
                                add = true;
                            }
                            break;
                    }

                    if (add) {
                        issuesReduced.add(issue);
                    }
                }
            } else {
                if (issue instanceof InvalidXMLIssue) {
                    triple = issuesToReport.get(IssueType.INVALID_XML);
                } else if (issue instanceof UncheckableMappingRuleException) {
                    triple = issuesToReport.get(IssueType.UNCHECKABLE_MAPPING);
                }

                if (triple != null && triple.report) {
                    issuesReduced.add(issue);
                }
            }
        }

        return new Iterator<Issue>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < issuesReduced.size() && issuesReduced.toArray()[currentIndex] != null;
            }

            @Override
            public Issue next() {
            	if(!hasNext())
            		throw new NoSuchElementException();
            	
                return (Issue) issuesReduced.toArray()[currentIndex++];
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    /**
     *
     * @param level
     */
    public final void setReportingLevel(ReportingLevel level) {
        issuesToReport = new EnumMap<>(IssueType.class);

        issuesToReport.put(IssueType.INVALID_XML, new IssueTriple(IssueType.INVALID_XML, true));
        issuesToReport.put(IssueType.INVALID_XML, new IssueTriple(IssueType.INVALID_XML, true));

        switch (level) {
            case FULL:
                issuesToReport.put(IssueType.UNCHECKABLE_MAPPING, new IssueTriple(IssueType.UNCHECKABLE_MAPPING, true));
            case ALMOSTFULL:
            case MAY:
                issuesToReport.put(IssueType.COMBINATION_MAPPING, new IssueTriple(IssueType.COMBINATION_MAPPING, RequirementLevel.MAY, true));
                issuesToReport.put(IssueType.CHILD_USED_MAPPING, new IssueTriple(IssueType.CHILD_USED_MAPPING, RequirementLevel.MAY, true));
                issuesToReport.put(IssueType.REPEATED_MAPPING, new IssueTriple(IssueType.REPEATED_MAPPING, RequirementLevel.MAY, true));
                issuesToReport.put(IssueType.USED_TERM_MAPPING, new IssueTriple(IssueType.USED_TERM_MAPPING, RequirementLevel.MAY, true));
                break;
            case SHOULD:
                issuesToReport.put(IssueType.COMBINATION_MAPPING, new IssueTriple(IssueType.COMBINATION_MAPPING, RequirementLevel.SHOULD, true));
                issuesToReport.put(IssueType.CHILD_USED_MAPPING, new IssueTriple(IssueType.CHILD_USED_MAPPING, RequirementLevel.SHOULD, true));
                issuesToReport.put(IssueType.REPEATED_MAPPING, new IssueTriple(IssueType.REPEATED_MAPPING, RequirementLevel.SHOULD, true));
                issuesToReport.put(IssueType.USED_TERM_MAPPING, new IssueTriple(IssueType.USED_TERM_MAPPING, RequirementLevel.SHOULD, true));
                break;
            case MUST:
                issuesToReport.put(IssueType.COMBINATION_MAPPING, new IssueTriple(IssueType.COMBINATION_MAPPING, RequirementLevel.MUST, true));
                issuesToReport.put(IssueType.CHILD_USED_MAPPING, new IssueTriple(IssueType.CHILD_USED_MAPPING, RequirementLevel.MUST, true));
                issuesToReport.put(IssueType.REPEATED_MAPPING, new IssueTriple(IssueType.REPEATED_MAPPING, RequirementLevel.MUST, true));
                issuesToReport.put(IssueType.USED_TERM_MAPPING, new IssueTriple(IssueType.USED_TERM_MAPPING, RequirementLevel.MUST, true));
                break;
        }
    }

    /**
     *
     * @param type
     * @param report
     */
    public void setIssueReporting(IssueType type, boolean report) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param type
     * @param requirementLevel
     * @param report
     */
    public void setIssueReporting(IssueType type, RequirementLevel requirementLevel, boolean report) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
