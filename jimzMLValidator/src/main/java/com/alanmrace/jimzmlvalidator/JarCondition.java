package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.mzml.MzML;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alan Race
 */
public class JarCondition implements Condition {
    Condition condition;
    
    public JarCondition(String jarLocation, String className) {
        File myJar = new File(jarLocation);
        
        if (className.startsWith("java.")) 
            throw new IllegalArgumentException("Invalid class name " + className);
        
        URLClassLoader child;
        try {
            child = new URLClassLoader(new URL[]{myJar.toURI().toURL()}, this.getClass().getClassLoader());

            Class<?> classToLoad = Class.forName(className, true, child);
            Object instance = classToLoad.newInstance();

            if(!(instance instanceof Condition))
                throw new IllegalArgumentException("Invalid class name " + className);
              
            condition = (Condition) instance;
        } catch (MalformedURLException | IllegalAccessException | IllegalArgumentException
                | SecurityException | ClassNotFoundException | InstantiationException ex) { 
            Logger.getLogger(ConditionalMappingHandler.class.getName()).log(Level.SEVERE, null, ex);

            throw new IllegalArgumentException("Invalid jar or class name", ex);
        } 
    }
    
    @Override
    public boolean check(MzML mzML) {        
        return condition.check(mzML);
    }
    
}
