package com.alanmrace.jimzmlvalidator;

import com.beust.jcommander.Parameter;
import java.util.List;

import static com.alanmrace.jimzmlvalidator.JimzMLValidator.DEFAULT_OPTIONS_FILE_NAME;

/**
 *
 * @author Alan Race
 */
public class JimzMLValidatorCommand {
    
    public enum OutputType {
        TEXT,
        JSON
    }
    
    @Parameter(names = {"--help", "-h"}, help = true)
    protected boolean help;

    @Parameter(names = {"--ignoreDefaultMappingFiles", "-i"}, description = "Do not include the default mapping files in the validation")
    protected boolean ignoreDefaultMappingFiles;
    
    @Parameter(names = {"--mapping", "-m"}, description = "Include mapping file (may be used more than once)")
    protected List<String> mappingFiles;
    
    @Parameter(names = {"--conditional", "-c"}, description = "Include conditional mapping file (may be used more than once)")
    protected List<String> conditionalMappingFiles;
    
    @Parameter(description = "File to validate", required = true)
    protected String fileToValidate;
    
    @Parameter(names = {"--output", "-o"}, description = "Output filepath")
    protected String output;
    
    @Parameter(names = {"--outputType", "-t"}, description = "Output type")
    protected OutputType outputType = OutputType.TEXT;

    @Parameter(names = {"--options-file"}, description = "imzMLValidator options file in JSON format")
    protected String optionsFile = DEFAULT_OPTIONS_FILE_NAME;

    @Parameter(names = {"--verbose", "-v"}, description = "Verbose output")
    protected boolean verbose = false;
    
}
