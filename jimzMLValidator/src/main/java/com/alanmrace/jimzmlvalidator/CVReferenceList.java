package com.alanmrace.jimzmlvalidator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Alan Race
 */
public class CVReferenceList implements Iterable<CVReference> {
    protected ArrayList<CVReference> referenceList;
    
    public CVReferenceList() {
    	referenceList = new ArrayList<>();
    }

    public void addCVReference(CVReference cvReference) {
        this.referenceList.add(cvReference);
    }
    
    public CVReference getCVReference(String id) {
        for(CVReference ref : referenceList) {
            if(ref.getIdentifier().equals(id)) {
                return ref;
            }
        }
        
        return null;
    }
    
    @Override
    public Iterator<CVReference> iterator() {
        return referenceList.iterator();
    }
}
